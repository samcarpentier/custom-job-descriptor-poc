const request = require('request');
const YAML = require('yamljs');
const config = require('./config');

function getFileContentFromGitLab(filePath) {
  var urlEncodedFilePath = filePath.replace(/\//g, '%2f');

  return new Promise((resolve, reject) => {
    request({
      url: 'https://gitlab.com/api/v4/projects/7987524/repository/files/' + urlEncodedFilePath + '?ref=master',
      headers: {
        'Private-Token': config.token
      },
      method: 'GET',
      proxy: null
    }, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        var encodedContent = JSON.parse(body).content;
        var content = Buffer.from(encodedContent, 'base64').toString('ascii');
        resolve(content);
      }
    });
  });
}

(async () => {
  var prodDescriptorYaml = await getFileContentFromGitLab('repository/production/descriptor.yml');
  var prodDescriptorObject = YAML.parse(prodDescriptorYaml);
  console.log(prodDescriptorObject);
})();

